#!/bin/sh

# Make it TAP compliant, see http://testanything.org/tap-specification.html
echo "1..2"

failed=0
step=1

docker build -t gitlab-accessibility .

desc="Verify Pa11y installed"
docker run gitlab-accessibility /node_modules/.bin/pa11y

if test $? -eq 0; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

desc="Verify Pa11y-CI installed"
docker run gitlab-accessibility /node_modules/.bin/pa11y-ci

if test $? -eq 0; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo

# Finish tests
count=$((step-1))
if [ $failed -ne 0 ]; then
  echo "Failed $failed/$count tests"
  exit 1
else
  echo "Passed $count tests"
fi
